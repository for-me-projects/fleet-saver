package site.apollonia.android.fleetdownloader.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.io.File;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import site.apollonia.android.fleetdownloader.BuildConfig;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.activity.MainActivity;

public class Utils {

    private static String shared_name = "site.apollonia.android.fleetDownlaoder";
    private static String pro_version = "pro_version";

    public static void hideKeyword(View v) {
        if (v == null)
            return;
        InputMethodManager mm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        mm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void shareText(Context context, String s) {
        if (context == null || s == null)
            return;
        Intent intent = new Intent();
        intent.setType("text/plain");
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, s);
        startActivity(context, intent);
    }

    public static void shareText(Context context, @StringRes int res) {
        if (context == null)
            return;
        shareText(context, context.getString(res));
    }

    public static void startActivity(Context context, Intent intent) {
        if (context == null || intent == null)
            return;
        try {
            context.startActivity(intent);
        }catch (Exception e){
            error(e);
        }
    }

    /**
     * Open url in other app
     * @param context
     * @param url Want to open
     */
    public static void openUrl(Context context, String url) {
        if (context == null || url == null)
            return;
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(context, intent);
    }

    public static void openUrl(Context context, @StringRes int res) {
        if (context == null)
            return;
        openUrl(context, context.getString(res));
    }

    public static void toast(@Nullable Context context, @Nullable String s) {
        if (context == null || s == null)
            return;
        try {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            error(e);
        }
    }

    public static void toast(@Nullable Context context, @StringRes int resId){
        if (context == null)
            return;
        toast(context, context.getString(resId));
    }

    public static void error(Exception e) {
        if (e == null)
            return;
        if (BuildConfig.DEBUG){
            e.printStackTrace();
        }
    }

    public static void loadAds(View adView) {
        if (! (adView instanceof AdView))
            return;
        if (adView.getContext().getSharedPreferences(Utils.shared_name, Context.MODE_PRIVATE).getBoolean(Utils.pro_version, false)) {
            adView.setVisibility(View.GONE);
        }else {
            ((AdView) adView).loadAd(new AdRequest.Builder().build());
        }
    }

    public static void loadAds(Activity activity) {
        if (activity == null)
            return;
        if (activity.getSharedPreferences(Utils.shared_name, Context.MODE_PRIVATE).getBoolean(Utils.pro_version, false))
            return;
        InterstitialAd.load(
                activity,
                activity.getString(R.string.admob_interstitial_id),
                new AdRequest.Builder().build(),
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        super.onAdLoaded(interstitialAd);
                        interstitialAd.show(activity);
                    }
                });
    }

    public static int randomName() {
        return new Random().nextInt(10000000);
    }

    public static boolean storageGranted(Context context) {
        if (context == null)
            return false;
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestStorage(Activity activity, int storage) {
        if (activity == null)
            return;
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, storage);
    }

    public static void log(String s) {
        if (s == null || ! BuildConfig.DEBUG)
            return;
        Log.i("fleet_saver", s);
    }

    public static void loadPhoto(ImageView iv, String url, boolean isCircle) {
        if (isCircle) {
            Glide
                    .with(iv)
                    .load(url)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .apply(new RequestOptions().circleCrop())
                    .into(iv);
        } else {
            Glide.with(iv).load(url).into(iv);
        }
    }

    public static void loadPhoto(ImageView iv, String url, Listener<Void> listener) {
        Glide
                .with(iv)
                .load(url)
                .transition(DrawableTransitionOptions.withCrossFade())
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        listener.data(null);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        listener.data(null);
                        return false;
                    }
                })
                .into(iv);
    }

    public static boolean deleteFile(String path) {
        if (path == null)
            return false;
        File file = new File(path);
        if (! file.exists())
            return false;
        return file.delete();
    }

    public static void shareFile(Context context, String path) {
        if (context == null || path == null)
            return;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = FileProvider.getUriForFile(
                context,
                context.getApplicationContext()
                        .getPackageName() + ".provider", new File(path));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        if (path.endsWith(".jpg")) {
            intent.setType("image/*");
        }else if (path.endsWith(".mp4")) {
            intent.setType("video/*");
        }
        startActivity(context, intent);
    }

    public static void openFile(Context context, String path) {
        if (context == null || path == null)
            return;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = FileProvider.getUriForFile(
                context,
                context.getApplicationContext()
                        .getPackageName() + ".provider", new File(path));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (path.endsWith(".jpg")) {
            intent.setDataAndType(uri, "image/*");
        }else if (path.endsWith(".mp4")) {
            intent.setDataAndType(uri, "video/*");
        }
        startActivity(context, intent);

    }

}
