package site.apollonia.android.fleetdownloader.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.activity.PhotoViewActivity;
import site.apollonia.android.fleetdownloader.activity.VideoPlayerActivity;
import site.apollonia.android.fleetdownloader.model.FileModel;
import site.apollonia.android.fleetdownloader.utils.AppDatabase;
import site.apollonia.android.fleetdownloader.utils.Constant;
import site.apollonia.android.fleetdownloader.utils.Utils;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.FileHolder> {

    private Context context;
    private List<FileModel> files;

    public FileAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public FileHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FileHolder(LayoutInflater.from(context).inflate(R.layout.file_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FileHolder holder, int position) {
        holder.bind(files.get(position));
    }

    @Override
    public int getItemCount() {
        if (files == null)
            return 0;
        else
            return files.size();
    }

    public void insert(List<FileModel> files) {
        if (files == null)
            return;
        if (this.files == null)
            this.files = new ArrayList<>();
        this.files.addAll(files);
        notifyDataSetChanged();
    }

    public void clearData() {
        if (files != null)
            files.clear();
        notifyDataSetChanged();
    }

    public void addFile(FileModel fm) {
        if (fm == null)
            return;
        if (files == null)
            files = new ArrayList<>();
        files.add(0, fm);
        notifyItemInserted(0);
    }

    class FileHolder extends RecyclerView.ViewHolder {
        private ImageView ivMain, ivPlay;
        private TextView tvTitle, tvInfo;

        public FileHolder(@NonNull View itemView) {
            super(itemView);
            ivMain = itemView.findViewById(R.id.imageView);
            ivPlay = itemView.findViewById(R.id.imageView_play);
            tvTitle = itemView.findViewById(R.id.textView_title);
            tvInfo = itemView.findViewById(R.id.textView_info);
            itemView.setOnClickListener(v -> {
                FileModel fm = files.get(getAdapterPosition());
                if (fm.type.equals(Constant.image_type)) {
                    Intent intent = new Intent(context, PhotoViewActivity.class);
                    intent.putExtra(PhotoViewActivity.fucking_image_url, fm.path);
                    Utils.startActivity(context, intent);
                }else if (fm.type.equals(Constant.video_type)) {
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra(VideoPlayerActivity.video_url, fm.path);
                    Utils.startActivity(context, intent);
                }
            });
            itemView.findViewById(R.id.imageButton).setOnClickListener(view -> showMenu(view));
        }

        void bind(FileModel fm) {
            Utils.loadPhoto(ivMain, fm.path, false);
            tvTitle.setText(fm.name);
            tvInfo.setText(fm.path);
            if (fm.type.equals(Constant.video_type))
                ivPlay.setVisibility(View.VISIBLE);
            else
                ivPlay.setVisibility(View.INVISIBLE);
        }

        private void showMenu(View view) {
            PopupMenu popupMenu = new PopupMenu(context, view);
            popupMenu.inflate(R.menu.file_menu);
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(menuItem -> {
                FileModel fm = files.get(getAdapterPosition());
                int itemId = menuItem.getItemId();
                if (itemId == R.id.file_delete) {
                    deleteFile(fm);
                } else if (itemId == R.id.file_share) {
                    Utils.shareFile(context, fm.path);
                } else if (itemId == R.id.file_openWith) {
                    Utils.openFile(context, fm.path);
                } else if (itemId == R.id.file_fleet) {

                }
                return true;
            });
        }

    }

    private void deleteFile(FileModel fm) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.delete);
        builder.setPositiveButton(R.string.delete, (dialogInterface, i) -> {
            Utils.deleteFile(fm.path);
            if (new AppDatabase(context).delete(fm.path)) {
                int index = -1;
                for (int j = 0; j < files.size(); j++) {
                    if (files.get(j).path.equals(fm.path)) {
                        index = j;
                        break;
                    }
                }
                if (index >= 0) {
                    files.remove(index);
                    notifyItemRemoved(index);
                }
            }else {
                Utils.toast(context, R.string.delete_failed);
            }
        });
        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.cancel());
        builder.setMessage(context.getString(R.string.do_you_want_to_delete_s_file, fm.name));
        builder.show();
    }

}
