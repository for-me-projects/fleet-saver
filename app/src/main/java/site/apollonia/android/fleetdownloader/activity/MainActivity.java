package site.apollonia.android.fleetdownloader.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.dialog.MenuBottomSheet;
import site.apollonia.android.fleetdownloader.fragment.FilesFragment;
import site.apollonia.android.fleetdownloader.fragment.HomeFragment;
import site.apollonia.android.fleetdownloader.fragment.TweetFragment;
import site.apollonia.android.fleetdownloader.fragment.UnsplashFragment;
import site.apollonia.android.fleetdownloader.utils.Utils;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    public static final int storage_code = 2001;

    private BottomNavigationView bnv;
    private HomeFragment homeFragment;
    private FilesFragment filesFragment;
    private UnsplashFragment unsplashFragment;
    private TweetFragment tweetFragment;
    private int lastFragmentId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        homeFragment = new HomeFragment();
        filesFragment = new FilesFragment();
        unsplashFragment = new UnsplashFragment();
        tweetFragment = new TweetFragment();
        bnv = findViewById(R.id.bottomNav);

        if (! Utils.storageGranted(this))
            Utils.requestStorage(this, storage_code);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24);
        Utils.loadAds(findViewById(R.id.adView));
        bnv.setOnNavigationItemSelectedListener(item -> navItemListener(item.getItemId()));
        navItemListener(R.id.nav_home);
    }

    private boolean navItemListener(int id) {
        if (lastFragmentId == id)
            return false;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_home) {
            if (homeFragment.isAdded())
                ft.show(homeFragment);
            else
                ft.add(R.id.frameLayout, homeFragment);
        }else if (id == R.id.nav_files) {
            if (filesFragment.isAdded())
                ft.show(filesFragment);
            else
                ft.add(R.id.frameLayout, filesFragment);
        }else if (id == R.id.nav_forFleet) {
            if (unsplashFragment.isAdded())
                ft.show(unsplashFragment);
            else
                ft.add(R.id.frameLayout, unsplashFragment);
        }else if (id == R.id.nav_tweet) {
            if (tweetFragment.isAdded())
                ft.show(tweetFragment);
            else
                ft.add(R.id.frameLayout, tweetFragment);
        }
        try {
            ft.commit();
        } catch (Exception e) {
            Utils.error(e);
        }
        hideFragment(lastFragmentId);
        lastFragmentId = id;
        return true;
    }

    private void hideFragment(int id) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_home) {
            ft.hide(homeFragment);
        }else if (id == R.id.nav_files) {
            ft.hide(filesFragment);
        }else if (id == R.id.nav_forFleet) {
            ft.hide(unsplashFragment);
        }else if (id == R.id.nav_tweet) {
            ft.hide(tweetFragment);
        }
        try {
            ft.commit();
        } catch (Exception e) {
            Utils.error(e);
        }
    }

    @Override
    public void onBackPressed() {
        if (lastFragmentId == R.id.nav_home) {
            if (homeFragment.canGoBack()) {
                super.onBackPressed();
                return;
            }
        }else if (lastFragmentId == R.id.nav_tweet) {
            if (tweetFragment.canGoBack()) {
                super.onBackPressed();
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.share).setIcon(R.drawable.ic_baseline_share_24).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            new MenuBottomSheet(id -> {
                if (id == R.id.button_share){
                    Utils.shareText(this, R.string.app_url);
                }else if (id == R.id.button_rate){
                    Utils.openUrl(this, R.string.rate_app_intent);
                }else if (id == R.id.button_upgrade){
                    // TODO: 3/8/21 upgrade section
                }
            }).show(getSupportFragmentManager(), null);
        }else if (item.getTitle() != null && item.getTitle().toString().equals(getString(R.string.share))) {
            Utils.shareText(this, R.string.app_url);
        }
        return super.onOptionsItemSelected(item);
    }

}