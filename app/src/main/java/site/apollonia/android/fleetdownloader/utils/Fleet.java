package site.apollonia.android.fleetdownloader.utils;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.OnProgressListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.fragment.FilesFragment;
import site.apollonia.android.fleetdownloader.model.FileModel;

public class Fleet {

//    public static void download(Context context, String url, String name) {
//        if (context == null || url == null || name == null)
//            return;
//        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//        request.setTitle(name);
//        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name);
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        request.setDescription("Fleet downloader");
//        request.setAllowedOverMetered(true);
//        request.setAllowedOverRoaming(true);
//        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//        manager.enqueue(request);
//    }

    public static int downloadWallpaper(Context context, String url) {
        return download(
                context,
                url,
                Utils.randomName()
                ,
                Constant.image_type,
                true,
                context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
        );
    }


    public static int downloadTweet(Context context, String url) {
        String type = null;
        if (url.contains("video.twimg.com")) {
            type = Constant.video_type;
        }
        return download(
                context,
                url,
                Utils.randomName(),
                type,
                false,
                context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
        );
    }

    public static int downloadFleet(Context context, String url) {
        String type = null;
        if (url.contains("download-image")) {
            type = Constant.image_type;
        }else if (url.contains("download-video")) {
            type = Constant.video_type;
        }
        return download(
                context,
                url,
                Utils.randomName(),
                type,
                false,
                context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
        );
    }

    static int download(Context context, String url, int notificationID, @Nullable String type, boolean isWallpaper, String path) {
        Utils.toast(context, R.string.download_started);

        String name;
        if (type == null)
            type = Constant.image_type;
        if (type.equals(Constant.image_type))
            name = notificationID + ".jpg";
        else if (type.equals(Constant.video_type))
            name = notificationID + ".mp4";
        else
            name = "test.a";

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "00");
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle(isWallpaper ? context.getString(R.string.download_wallpaper) : context.getString(R.string.download_fleet));
        builder.setProgress(100, 0, true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, builder.build());

        String finalType = type;
        return PRDownloader
                .download(url, path, name)
                .build()
                .setOnProgressListener(progress -> {
                    int per = (int) ((progress.currentBytes / progress.totalBytes) * 100);
                    builder.setProgress(100, per, false);
                    notificationManager.notify(notificationID, builder.build());
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        FileModel f = new FileModel();
                        f.name = name;
                        f.url = url;
                        f.path = path + "/" + name;
                        f.isWallpaper = isWallpaper;
                        f.type = finalType;
                        new AppDatabase(context).insert(f);
                        builder.setContentText(context.getString(R.string.download_complete));
                        notificationManager.notify(notificationID, builder.build());

                        Intent intent = new Intent(FilesFragment.action_new_file);
                        intent.putExtra(Intent.EXTRA_STREAM, f);
                        context.sendBroadcast(intent);
                    }

                    @Override
                    public void onError(Error error) {
                        Utils.toast(context, error.getServerErrorMessage());
                        builder.setContentText(context.getString(R.string.please_try_again));
                        notificationManager.notify(notificationID, builder.build());
                    }
                });
    }

}
