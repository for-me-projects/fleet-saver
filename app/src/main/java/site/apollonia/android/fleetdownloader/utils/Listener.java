package site.apollonia.android.fleetdownloader.utils;

public interface Listener<T> {

    void data(T t);

}
