package site.apollonia.android.fleetdownloader.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import limitless.android.unsplash.Api.Model.Photo;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.activity.PhotoViewActivity;
import site.apollonia.android.fleetdownloader.utils.Listener;
import site.apollonia.android.fleetdownloader.utils.Utils;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

    private Context context;
    private List<Photo> photos;
    private Listener<Void> scrollEndListener;

    public PhotoAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(context).inflate(R.layout.photo_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        holder.bindView(photos.get(position));
    }

    @Override
    public int getItemCount() {
        if (photos == null)
            return 0;
        else
            return photos.size();
    }

    public void addPhotos(List<Photo> photos) {
        if (photos == null)
            return;
        if (this.photos == null)
            this.photos = new ArrayList<>();
        this.photos.addAll(photos);
        notifyDataSetChanged();
    }

    public void setScrollEndListener(Listener<Void> scrollEndListener) {
        this.scrollEndListener = scrollEndListener;
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv;
        private TextView tv;
        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.imageView);
            tv = itemView.findViewById(R.id.textView_title);
            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(context, PhotoViewActivity.class);
                intent.putExtra(PhotoViewActivity.fucking_image_url, photos.get(getAdapterPosition()).getUrls().getFull());
                Utils.startActivity(context, intent);
            });
        }

        void bindView(Photo photo) {
            Utils.loadPhoto(iv, photo.getUrls().getSmall(), false);
            tv.setText(photo.getUser().getName());

            if (photos.size() - 1 == getAdapterPosition() && scrollEndListener != null)
                scrollEndListener.data(null);
        }

    }

}
