package site.apollonia.android.fleetdownloader.utils;

public class Constant {

    // images for per page request, unsplash
    public static final int per_page_photo = 45;
    // timeout connection
    public static final int connection_time_out = 3000;
    // file type for video
    public static final String video_type = "video";
    // file type for image
    public static final String image_type = "image";
    // delay time for video player
    public static long delaMills = 100;
    // android:authorities
    public static String authorities = "site.apollonia.android.fleetdownloader.fileprovider";
    // show page after load
    public static final long load_page_time = 2000;

}
