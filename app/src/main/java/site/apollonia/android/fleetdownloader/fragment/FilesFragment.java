package site.apollonia.android.fleetdownloader.fragment;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.recyclerview.widget.RecyclerView;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.adapter.FileAdapter;
import site.apollonia.android.fleetdownloader.model.FileModel;
import site.apollonia.android.fleetdownloader.utils.AppDatabase;
import site.apollonia.android.fleetdownloader.utils.Utils;

public class FilesFragment extends BaseFragment {

    private AppDatabase appDatabase;
    private RecyclerView rv;
    private FileAdapter fileAdapter;
    private TabLayout tabLayout;
    private ProgressBar progressBar;
    private View noFile;
    private boolean isWallpaper = false;
    public static final String action_new_file = "action_new_fucking_file";
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().endsWith(action_new_file)) {
                // add new file
                FileModel fm = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (fm == null)
                    return;
                if (isWallpaper) {
                    if (fm.isWallpaper)
                        fileAdapter.addFile(fm);
                }else {
                    if (! fm.isWallpaper)
                        fileAdapter.addFile(fm);
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_files, container, false);
        inti(view);
        return view;
    }

    private void inti(View view) {
        appDatabase = new AppDatabase(getContext());
        rv = view.findViewById(R.id.recyclerView);
        fileAdapter = new FileAdapter(getContext());
        tabLayout = view.findViewById(R.id.tabLayout);
        noFile = view.findViewById(R.id.noFile);
        progressBar = view.findViewById(R.id.progressBar);

        IntentFilter intentFilter = new IntentFilter(action_new_file);
        getContext().registerReceiver(broadcastReceiver, intentFilter);
        rv.setAdapter(fileAdapter);
        getData(false);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getId() == R.id.tab_fleets) {
                    getData(false);
                }else if (tab.getId() == R.id.tab_wallpaper) {
                    getData(true);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Utils.log(tab.getId() + " --- " + R.id.tab_wallpaper);
                if (tabLayout.getSelectedTabPosition() == 0) {
                    getData(false);
                }else if (tabLayout.getSelectedTabPosition() == 1) {
                    getData(true);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Utils.log(tab.getText().toString());
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }

    private void getData(boolean isWallpaper) {
        this.isWallpaper = isWallpaper;
        progressBar.setVisibility(View.VISIBLE);
        fileAdapter.clearData();
        Executor executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> {
            List<FileModel> files = appDatabase.get(isWallpaper);
            handler.post(() -> {
                if (files == null || files.size() <= 0) {
                    noFile.setVisibility(View.VISIBLE);
                    rv.setVisibility(View.INVISIBLE);
                    return;
                }else {
                    noFile.setVisibility(View.INVISIBLE);
                    rv.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.INVISIBLE);
                fileAdapter.clearData();
                fileAdapter.insert(files);
            });
        });
    }
}