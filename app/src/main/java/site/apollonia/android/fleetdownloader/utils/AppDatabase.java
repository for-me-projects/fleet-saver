package site.apollonia.android.fleetdownloader.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import site.apollonia.android.fleetdownloader.model.FileModel;

public class AppDatabase extends SQLiteOpenHelper {

    private static final String name = "fleet.database";
    private static final int version = 1;
    private SQLiteDatabase database;

    private String dTable = "CREATE TABLE `downloads` (\n" +
            "\t`name`\tTEXT NOT NULL,\n" +
            "\t`path`\tTEXT NOT NULL,\n" +
            "\t`type`\tTEXT NOT NULL,\n" +
            "\t`isWallpaper`\tINTEGER NOT NULL,\n" +
            "\t`url`\tTEXT NOT NULL,\n" +
            "\tPRIMARY KEY(`path`)\n" +
            ");";
    private String downloads = "downloads";
    private String dName = "name";
    private String dPath = "path";
    private String dType = "type";
    private String dWallpaper = "isWallpaper";
    private String dUrl = "url";


    public AppDatabase(@Nullable Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(dTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private void read(){
        try {
            database = getReadableDatabase();
        } catch (Exception e) {
            Utils.error(e);
        }
    }

    private void write(){
        try {
            database = getWritableDatabase();
        } catch (Exception e) {
            Utils.error(e);
        }
    }

    public boolean insert(FileModel fm) {
        if (fm == null)
            return false;
        write();
        ContentValues cv = new ContentValues();
        cv.put(dName, fm.name);
        cv.put(dPath, fm.path);
        cv.put(dUrl, fm.url);
        cv.put(dType, fm.type);
        cv.put(dWallpaper, fm.isWallpaper ? 1 : 0);
        return database.insert(downloads, null, cv) > 0;
    }
    
    public List<FileModel> get(boolean isWallpaper) {
        read();
        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + downloads + " WHERE " + dWallpaper + "=" + (isWallpaper ? 1 : 0),
                null);
        if (cursor == null)
            return null;
        if (cursor.getCount() <= 0 || ! cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        int n = cursor.getColumnIndex(dName);
        int p = cursor.getColumnIndex(dPath);
        int u = cursor.getColumnIndex(dUrl);
        int t = cursor.getColumnIndex(dType);
        int w = cursor.getColumnIndex(dWallpaper);
        List<FileModel> files = new ArrayList<>();
        cursor.moveToFirst();
        do {
            files.add(new FileModel(
                    cursor.getString(n),
                    cursor.getString(p),
                    cursor.getString(u),
                    cursor.getString(t),
                    cursor.getInt(w) == 1 // true or false
            ));
        }while (cursor.moveToNext());
        cursor.close();
        List<FileModel> list = new ArrayList<>();
        int fSize = files.size();
        for (int i = 0; i < fSize; i++) {
            list.add(files.get(fSize - i - 1));
        }
        return list;
    }
    
    public boolean delete(String path) {
        if (path == null)
            return false;
        write();
        return database.delete(
                    downloads,
                    dPath + "=?",
                    new String[]{path}
            ) > 0;
    }
    

}
