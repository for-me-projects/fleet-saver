package site.apollonia.android.fleetdownloader.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import limitless.android.unsplash.Api.Enum.Order;
import limitless.android.unsplash.Api.Model.Photo;
import limitless.android.unsplash.Listeners.UnsplashListener;
import limitless.android.unsplash.Unsplash;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.adapter.PhotoAdapter;
import site.apollonia.android.fleetdownloader.utils.Constant;
import site.apollonia.android.fleetdownloader.utils.Listener;
import site.apollonia.android.fleetdownloader.utils.Utils;

public class UnsplashFragment extends BaseFragment {

    // current page
    private int page = 1;
    private Unsplash unsplash;
    private RecyclerView rv;
    private ProgressBar pb;
    private View errorView;
    private PhotoAdapter photoAdapter;

    private UnsplashListener<List<Photo>> photoListener = new UnsplashListener<List<Photo>>() {
        @Override
        public void data(List<Photo> data) {
            photoAdapter.addPhotos(data);
            errorView.setVisibility(View.INVISIBLE);
            pb.setVisibility(View.INVISIBLE);
        }

        @Override
        public void error(String error) {
            if (photoAdapter.getItemCount() <= 0){
                errorView.setVisibility(View.VISIBLE);
                Utils.toast(getContext(), error);
            }
        }
    };
    private Listener<Void> scrollEndListener = new Listener<Void>() {
        @Override
        public void data(Void aVoid) {
            page += 1;
            pb.setVisibility(View.VISIBLE);
            unsplash.getPhotos(page, Constant.per_page_photo, Order.LATEST.getOrder(), photoListener);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unsplash, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        rv = view.findViewById(R.id.recyclerView);
        pb = view.findViewById(R.id.progressBar);
        errorView = view.findViewById(R.id.errorView);
        unsplash = new Unsplash(getString(R.string.unsplash_access_key), false);
        photoAdapter = new PhotoAdapter(getContext());

        rv.setAdapter(photoAdapter);
        rv.setLayoutManager(new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false));
        photoAdapter.setScrollEndListener(scrollEndListener);
        unsplash.getPhotos(page, Constant.per_page_photo, Order.LATEST.getOrder(), photoListener);
    }



}