package site.apollonia.android.fleetdownloader.activity;

import androidx.appcompat.app.AppCompatActivity;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.utils.Constant;
import site.apollonia.android.fleetdownloader.utils.Utils;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

public class VideoPlayerActivity extends AppCompatActivity {

    public static final String video_url = "video_url";

    private String url;
    private VideoView videoView;
    private ProgressBar progressBar;
    private ImageButton imageButton;
    private Handler handler;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (videoView.isPlaying()) {
                progressBar.setProgress(videoView.getCurrentPosition());
                handler.postDelayed(this, Constant.delaMills);
            }
        }
    };
    private MediaPlayer.OnPreparedListener preparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            handler.postDelayed(runnable, Constant.delaMills);
            imageButton.setImageResource(R.drawable.ic_baseline_pause_24);
            progressBar.setMax(videoView.getDuration());
        }
    };
    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            imageButton.setImageResource(R.drawable.ic_baseline_play_arrow_24);
            progressBar.setProgress(0);
        }
    };
    private MediaPlayer.OnErrorListener errorListener = (mediaPlayer, i, i1) -> {
        Utils.toast(VideoPlayerActivity.this, R.string.error);
        return true;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (videoView.isPlaying()) {
                videoView.pause();
            }
            videoView.resume();
        }catch (Exception e){
            Utils.error(e);
        }
    }

    private void init() {
        url = getIntent().getStringExtra(video_url);
        if (url == null) {
            finish();
            return;
        }
        handler = new Handler(getMainLooper());
        videoView = findViewById(R.id.videoView);
        progressBar = findViewById(R.id.progressBar);
        imageButton = findViewById(R.id.imageButton_play);

        videoView.setVideoPath(url);
        videoView.setOnPreparedListener(preparedListener);
        videoView.setOnCompletionListener(completionListener);
        videoView.setOnErrorListener(errorListener);
        videoView.start();
        findViewById(R.id.imageButton).setOnClickListener(view -> finish());
        imageButton.setOnClickListener(view -> {
            if (videoView.isPlaying()) {
                videoView.pause();
                imageButton.setImageResource(R.drawable.ic_baseline_play_arrow_24);
            }else {
                videoView.start();
                imageButton.setImageResource(R.drawable.ic_baseline_pause_24);
                handler.postDelayed(runnable, Constant.delaMills);
            }

        });
    }

}