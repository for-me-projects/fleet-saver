package site.apollonia.android.fleetdownloader.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FileModel implements Parcelable {

    // file name
    public String name;

    // file path
    public String path;

    // file url
    public String url;

    // file type video or image
    public String type;

    // downloaded from wallpapers or fleets
    public boolean isWallpaper;

    public FileModel() {

    }

    public FileModel(String name, String path, String url, String type, boolean isWallpaper) {
        this.name = name;
        this.path = path;
        this.url = url;
        this.type = type;
        this.isWallpaper = isWallpaper;
    }

    protected FileModel(Parcel in) {
        name = in.readString();
        path = in.readString();
        url = in.readString();
        type = in.readString();
        isWallpaper = in.readByte() != 0;
    }

    public static final Creator<FileModel> CREATOR = new Creator<FileModel>() {
        @Override
        public FileModel createFromParcel(Parcel in) {
            return new FileModel(in);
        }

        @Override
        public FileModel[] newArray(int size) {
            return new FileModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(path);
        parcel.writeString(url);
        parcel.writeString(type);
        parcel.writeByte((byte) (isWallpaper ? 1 : 0));
    }
}
