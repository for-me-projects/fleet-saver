package site.apollonia.android.fleetdownloader.utils;

public interface MenuClickListener {
    /**
     * @param id Button id
     */
    void menu(int id);
}
