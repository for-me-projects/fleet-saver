package site.apollonia.android.fleetdownloader;

import android.app.Application;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;

import java.util.Collections;

import site.apollonia.android.fleetdownloader.utils.Constant;

public class ApplicationLoader extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // init admob
        if (BuildConfig.DEBUG){ // Show test ads when is debug version
            RequestConfiguration rc = new RequestConfiguration
                    .Builder().setTestDeviceIds(Collections.singletonList("1959AA4068493046177228B46CB92270"))
                    .build();
            MobileAds.setRequestConfiguration(rc);
        }
        MobileAds.initialize(getApplicationContext());

        // init prdownloader
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder().build();
        config.setDatabaseEnabled(true);
        config.setReadTimeout(Constant.connection_time_out);
        config.setConnectTimeout(Constant.connection_time_out);
        PRDownloader.initialize(this, config);
    }
}
