package site.apollonia.android.fleetdownloader.fragment;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.activity.MainActivity;
import site.apollonia.android.fleetdownloader.utils.Constant;
import site.apollonia.android.fleetdownloader.utils.Fleet;
import site.apollonia.android.fleetdownloader.utils.Utils;

import static site.apollonia.android.fleetdownloader.activity.MainActivity.storage_code;

public class HomeFragment extends BaseFragment {

    private WebView webView;
    private ProgressBar pbLoad, pbFleets;
    private MaterialButton btnFleet;
    private View vMain;
    private TextInputEditText editText;

    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                webView.loadUrl(request.getUrl().getPath());
            }
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            webView.loadUrl(url);
            removeSections();
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            removeSections();

            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                webView.setVisibility(View.VISIBLE);
                vMain.setVisibility(View.INVISIBLE);
                btnFleet.setVisibility(View.VISIBLE);
                pbFleets.setVisibility(View.VISIBLE);
            }, Constant.load_page_time);
        }
    };
    private WebChromeClient webChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            pbLoad.setProgress(newProgress);
            if (newProgress >= 100) {
                pbLoad.setVisibility(View.INVISIBLE);
            }else {
                pbLoad.setVisibility(View.VISIBLE);
            }
        }
    };
    private DownloadListener downloadListener = (url, s1, s2, s3, l) -> {
        if (Utils.storageGranted(getContext())) {
            Fleet.downloadFleet(getContext(), url);
        }else {
            Utils.requestStorage(getActivity(), storage_code);
        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init(View view) {
        webView = view.findViewById(R.id.webView);
        pbLoad = view.findViewById(R.id.progressBarLinear);
        pbFleets = view.findViewById(R.id.progressBarGet);
        btnFleet = view.findViewById(R.id.button_getFleets);
        vMain = view.findViewById(R.id.view_main);
        editText = view.findViewById(R.id.editText);

//        webView.loadUrl("file:///android_asset/index.html");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebChromeClient(webChromeClient);
        btnFleet.setOnClickListener(v -> getFleets());
        webView.setDownloadListener(downloadListener);
        webView.setWebViewClient(webViewClient);
    }

    @Override
    public boolean canGoBack() {
        if (vMain.getVisibility() == View.VISIBLE) {
            return true; // go back
        } else {
            webView.setVisibility(View.INVISIBLE);
            vMain.setVisibility(View.VISIBLE);
            return false;
        }
    }

    // get fleets
    private void getFleets() {
        Utils.hideKeyword(editText);
        String s = editText.getText().toString();

        if (s.length() <= 0) {
            Utils.toast(getContext(), R.string.error);
            return;
        }
        Utils.loadAds(getActivity());
        webView.loadUrl("https://ddlvid.com/download?link=" + "https://twitter.com/" + s.replace("@", ""));
        btnFleet.setVisibility(View.INVISIBLE);
        pbFleets.setVisibility(View.VISIBLE);
    }

    private void removeSections() {
        webView.loadUrl("javascript:(function() { "
//                    + " var main = document.getElementById('content');"
                // remove header
                + " document.getElementById('header').remove();"
                // remove footer
                + " document.getElementById('footer').remove();"
                // remove download link
                + " document.getElementById('link').remove();"
                // remove shares
                + " document.getElementsByClassName('video_infos')[0].remove();"
                // remove share social
                + " document.getElementById('sharethis').remove();"
                // remove share items
                + " document.getElementById('sharethis-1615136031877').remove();"
//                    // remove sections
                + " var ads = document.getElementsByClassName('mgbox')[0]; ads.remove();"
                // remove downlaod section
                + " document.getElementsByClassName('jsx-3105550290 downloadForm')[0].remove();"
                + "})()");
    }

}