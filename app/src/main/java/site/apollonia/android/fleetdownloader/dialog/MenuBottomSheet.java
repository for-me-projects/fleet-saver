package site.apollonia.android.fleetdownloader.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.utils.MenuClickListener;

public class MenuBottomSheet extends BottomSheetDialogFragment {

    private MenuClickListener listener;

    public MenuBottomSheet(MenuClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.bottom_sheet_menu, container);
        getDialog().setContentView(view);
        view.findViewById(R.id.button_upgrade).setOnClickListener(this::menuClick);
        view.findViewById(R.id.button_share).setOnClickListener(this::menuClick);
        view.findViewById(R.id.button_rate).setOnClickListener(this::menuClick);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void menuClick(View v) {
        if (v == null || listener == null)
            return;
        listener.menu(v.getId());
        dismiss();
    }
}
