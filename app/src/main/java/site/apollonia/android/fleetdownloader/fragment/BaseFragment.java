package site.apollonia.android.fleetdownloader.fragment;

import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    /**
     * @return Can go back or no
     */
    public boolean canGoBack() {
        return true;
    }

}
