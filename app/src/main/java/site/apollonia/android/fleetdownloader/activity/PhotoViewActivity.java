package site.apollonia.android.fleetdownloader.activity;

import androidx.appcompat.app.AppCompatActivity;
import site.apollonia.android.fleetdownloader.R;
import site.apollonia.android.fleetdownloader.utils.Fleet;
import site.apollonia.android.fleetdownloader.utils.Listener;
import site.apollonia.android.fleetdownloader.utils.Utils;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.io.File;

import static site.apollonia.android.fleetdownloader.activity.MainActivity.storage_code;

public class PhotoViewActivity extends AppCompatActivity {

    public static final String fucking_image_url = "image_url";

    private String url;
    private ImageView iv;
    private ProgressBar pb;
    private ImageButton ibBack;
    private TextView tvTitle;
    private ExtendedFloatingActionButton efab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view_acitvity);
        init();
    }

    private void init() {
        url = getIntent().getStringExtra(fucking_image_url);
        if (url == null){
            finish();
            return;
        }
        iv = findViewById(R.id.imageView);
        pb = findViewById(R.id.progressBar);
        ibBack = findViewById(R.id.imageButton);
        tvTitle = findViewById(R.id.textView_title);
        efab = findViewById(R.id.efab);

        if (new File(url).exists())
            efab.setVisibility(View.GONE);
        ibBack.setOnClickListener(view -> finish());
        efab.setOnClickListener(view -> download());
        Utils.loadPhoto(iv, url, aVoid -> pb.setVisibility(View.INVISIBLE));
    }

    private void download() {
        if (Utils.storageGranted(this)) {
        Fleet.downloadWallpaper(this, url);
        }else {
            Utils.requestStorage(this, storage_code);
        }
    }

}